from utils.dir_utils import check_and_make_dir
from utils.queue_utils import queue_to_list
from class_package.consumer import Consumer
from class_package.producer import Producer
from queue import LifoQueue
from config import Config
import cv2


def main() -> None:
    config = Config()

    stack_a = LifoQueue(maxsize=config.get_stack_a_size())
    stack_b = LifoQueue(maxsize=config.get_stack_b_size())

    producer = Producer(stack_a)
    consumer = Consumer(stack_a, stack_b)

    while not stack_b.full():
        pass

    producer.stop()
    consumer.stop()

    list_b = queue_to_list(stack_b)
    check_and_make_dir(config.dir_save)

    for idx, img in enumerate(list_b):
        cv2.imwrite(config.get_image_path(index=idx), img)


if __name__ == "__main__":
    main()
