from dataclasses import dataclass


@dataclass
class Config:
    dir_save: str = "processed"
    stack_a_size: int = 0
    stack_b_size: int = 100
    image_name: str = "image_"
    image_format: str = "png"

    def get_stack_a_size(self) -> int:
        return self.stack_a_size if self.stack_a_size >= 0 else 0

    def get_stack_b_size(self) -> int:
        return self.stack_b_size if self.stack_b_size > 0 else 1

    def get_image_path(self, index: int) -> str:
        return self.dir_save + "/" + self.image_name + "{}.".format(index) + self.get_image_format()

    def get_image_format(self) -> str:
        return self.image_format if self.image_format in ["png", "jpg", "jpeg"] else "png"
