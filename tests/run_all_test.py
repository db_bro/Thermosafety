import thread_enlargement_test
import queue_utils_test
import dir_utils_test
import consumer_test
import producer_test
import config_test
import main_test
import unittest

tests = [config_test, consumer_test, dir_utils_test, main_test,
         producer_test, queue_utils_test, thread_enlargement_test]

for test in tests:
    suite = unittest.TestLoader().loadTestsFromModule(test)
    unittest.TextTestRunner(verbosity=2).run(suite)
