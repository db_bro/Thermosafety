from utils.queue_utils import *
import numpy as np
import unittest


class TestQueueUtils(unittest.TestCase):

    def setUp(self):
        self.lifo_queue = LifoQueue()

    def test_queue_to_list_empty(self):
        lifo_list = queue_to_list(self.lifo_queue)
        self.assertEqual(True, isinstance(lifo_list, list))

    def test_queue_to_list_int(self):
        self.lifo_queue.put(1)
        lifo_list = queue_to_list(self.lifo_queue)
        self.assertEqual(True, isinstance(lifo_list, list))

    def test_queue_to_list_float(self):
        self.lifo_queue.put(0.534)
        lifo_list = queue_to_list(self.lifo_queue)
        self.assertEqual(True, isinstance(lifo_list, list))

    def test_queue_to_list_array(self):
        self.lifo_queue.put(np.asarray([4]))
        lifo_list = queue_to_list(self.lifo_queue)
        self.assertEqual(True, isinstance(lifo_list, list))

    def test_queue_to_list_mix(self):
        self.lifo_queue.put(np.asarray([4]))
        self.lifo_queue.put(np.asarray([4, 3]))
        self.lifo_queue.put(np.asarray([[4], [3, 4]], dtype=object))
        self.lifo_queue.put(1)
        self.lifo_queue.put(-2352.252352)
        self.lifo_queue.put("test")
        lifo_list = queue_to_list(self.lifo_queue)
        self.assertEqual(True, isinstance(lifo_list, list))
