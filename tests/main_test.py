from config import Config
from os import path, walk
from main import main
import numpy as np
import unittest
import shutil
import cv2


class TestMain(unittest.TestCase):

    def test_main(self):
        config = Config()
        main()

        self.assertEqual(True, path.exists(config.dir_save))

        _, _, files = next(walk(config.dir_save + "/"))
        self.assertEqual(config.stack_b_size, len(files))

        img = cv2.imread(config.get_image_path(0))
        self.assertEqual(True, isinstance(img, np.ndarray))

        shutil.rmtree(config.dir_save)
