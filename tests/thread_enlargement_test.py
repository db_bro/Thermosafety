from class_package.thread_enlargement import ThreadEnlargement
from queue import LifoQueue
import unittest


class TestThreadEnlargement(unittest.TestCase):

    def setUp(self):
        self.thread_enlargement = ThreadEnlargement(LifoQueue(), thread_name="ThreadEnlargement")

    def test_init(self):
        self.assertEqual(False, self.thread_enlargement._stop_flag)
        self.assertEqual("ThreadEnlargement", self.thread_enlargement.name)
        self.assertEqual(True, self.thread_enlargement.daemon)

    def test_stop(self):
        self.thread_enlargement.stop()
        self.assertEqual(True, self.thread_enlargement._stop_flag)
