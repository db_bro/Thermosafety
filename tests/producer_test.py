from class_package.producer import Producer
from queue import LifoQueue
import unittest
import time


class TestProducer(unittest.TestCase):

    def setUp(self):
        self.producer = Producer(LifoQueue())

    def test_init(self):
        self.assertEqual(0.05, self.producer._time_send)
        self.assertEqual((1024, 768, 3), self.producer._source.get_data().shape)

    def test_change_time_send_int_positive(self):
        self.producer.change_time_send(324)
        self.assertEqual(324, self.producer._time_send)

    def test_change_time_send_int_negative(self):
        self.producer.change_time_send(-324)
        self.assertEqual(1, self.producer._time_send)

    def test_change_time_send_float_positive_big(self):
        self.producer.change_time_send(3244534.32)
        self.assertEqual(3244534.32, self.producer._time_send)

    def test_change_time_send_float_positive_small(self):
        self.producer.change_time_send(0.01)
        self.assertEqual(0.01, self.producer._time_send)

    def test_change_time_send_float_negative_big(self):
        self.producer.change_time_send(-32423532.54)
        self.assertEqual(1, self.producer._time_send)

    def test_change_time_send_float_negative_small(self):
        self.producer.change_time_send(-0.54)
        self.assertEqual(1, self.producer._time_send)

    def test_run_stop(self):
        stack_a = LifoQueue(maxsize=11)
        producer = Producer(stack_a, source_shape=(100, 100, 1))
        time.sleep(0.5)
        producer.stop()
        time.sleep(0.5)
        self.assertEqual(False, producer.is_alive())
