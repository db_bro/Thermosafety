from config import Config
import unittest


class TestConfig(unittest.TestCase):

    def setUp(self):
        self.config = Config()

    def test_init(self):
        self.assertEqual("processed", self.config.dir_save)
        self.assertEqual(0, self.config.stack_a_size)
        self.assertEqual(100, self.config.stack_b_size)
        self.assertEqual("image_", self.config.image_name)
        self.assertEqual("png", self.config.image_format)

    def test_init_with_dir_save(self):
        config = Config(dir_save="test")
        self.assertEqual("test", config.dir_save)

    def test_init_with_stack_a_size(self):
        config = Config(stack_a_size=22222)
        self.assertEqual(22222, config.stack_a_size)

    def test_init_with_stack_b_size(self):
        config = Config(stack_b_size=33333)
        self.assertEqual(33333, config.stack_b_size)

    def test_init_with_image_name(self):
        config = Config(image_name="test")
        self.assertEqual("test", config.image_name)

    def test_init_with_image_format(self):
        config = Config(image_format="jpg")
        self.assertEqual("jpg", config.image_format)

    def test_get_stack_a_size_init(self):
        self.assertEqual(0, self.config.get_stack_a_size())

    def test_get_stack_a_size_positive(self):
        self.config.stack_a_size = 55555
        self.assertEqual(55555, self.config.get_stack_a_size())

    def test_get_stack_a_size_negative(self):
        self.config.stack_a_size = -55555
        self.assertEqual(0, self.config.get_stack_a_size())

    def test_get_stack_b_size_init(self):
        self.assertEqual(100, self.config.get_stack_b_size())

    def test_get_stack_b_size_positive(self):
        self.config.stack_b_size = 4444
        self.assertEqual(4444, self.config.get_stack_b_size())

    def test_get_stack_b_size_negative(self):
        self.config.stack_b_size = -22
        self.assertEqual(1, self.config.get_stack_b_size())

    def test_get_image_path_init(self):
        self.assertEqual("processed/image_0.png", self.config.get_image_path(0))

    def test_get_image_path_init_with_dir_save(self):
        self.config.dir_save = "test"
        self.assertEqual("test/image_0.png", self.config.get_image_path(0))

    def test_get_image_path_init_with_image_name(self):
        self.config.image_name = "test_"
        self.assertEqual("processed/test_0.png", self.config.get_image_path(0))

    def test_get_image_path_init_with_image_format(self):
        self.config.image_format = "jpeg"
        self.assertEqual("processed/image_0.jpeg", self.config.get_image_path(0))

    def test_get_image_path_mix(self):
        self.config.image_name = "test_"
        self.config.dir_save = "test"
        self.config.image_format = "jpeg"
        self.assertEqual("test/test_0.jpeg", self.config.get_image_path(0))

    def test_get_image_format_init(self):
        self.assertEqual("png", self.config.get_image_format())

    def test_get_image_format_jpg(self):
        self.config.image_format = "jpg"
        self.assertEqual("jpg", self.config.get_image_format())

    def test_get_image_format_jpeg(self):
        self.config.image_format = "jpeg"
        self.assertEqual("jpeg", self.config.get_image_format())

    def test_get_image_format_random(self):
        self.config.image_format = "test"
        self.assertEqual("png", self.config.get_image_format())
