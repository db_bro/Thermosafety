from utils.dir_utils import check_and_make_dir
from os import path
import unittest
import os.path


class TestDirUtils(unittest.TestCase):

    def test_check_and_make_dir(self):
        check_and_make_dir("test_check_and_make_dir")
        self.assertEqual(True, path.exists("test_check_and_make_dir"))
        os.rmdir("test_check_and_make_dir")
