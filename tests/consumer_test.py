from class_package.consumer import Consumer
from class_package.source import Source
from queue import LifoQueue
import unittest
import time


class TestConsumer(unittest.TestCase):

    def setUp(self):
        self.consumer = Consumer(LifoQueue(), LifoQueue())

    def test_init(self):
        self.assertEqual(2, self.consumer._image_compress_factor)
        self.assertEqual(5, self.consumer._kernel_median_size)

    def test_change_median_kernel_size_init(self):
        self.assertEqual(5, self.consumer._kernel_median_size)

    def test_change_median_kernel_size_change_positive_even(self):
        self.consumer.change_median_kernel_size(436346346)
        self.assertEqual(436346347, self.consumer._kernel_median_size)

    def test_change_median_kernel_size_change_positive_odd(self):
        self.consumer.change_median_kernel_size(17)
        self.assertEqual(17, self.consumer._kernel_median_size)

    def test_change_median_kernel_size_change_to_0_even(self):
        self.consumer.change_median_kernel_size(-0)
        self.assertEqual(1, self.consumer._kernel_median_size)

    def test_change_median_kernel_size_change_to_0_odd(self):
        self.consumer.change_median_kernel_size(0)
        self.assertEqual(1, self.consumer._kernel_median_size)

    def test_change_median_kernel_size_change_to_negative_even(self):
        self.consumer.change_median_kernel_size(-532525234)
        self.assertEqual(1, self.consumer._kernel_median_size)

    def test_change_median_kernel_size_change_to_negative_odd(self):
        self.consumer.change_median_kernel_size(-9999999)
        self.assertEqual(1, self.consumer._kernel_median_size)

    def test_change_image_compress_factor_init(self):
        self.assertEqual(2, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_positive_even(self):
        self.consumer.change_image_compress_factor(436346346)
        self.assertEqual(436346346, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_positive_odd(self):
        self.consumer.change_image_compress_factor(17)
        self.assertEqual(17, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_0_even(self):
        self.consumer.change_image_compress_factor(-0)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_0_odd(self):
        self.consumer.change_image_compress_factor(0)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_negative_even(self):
        self.consumer.change_image_compress_factor(-532525234)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_negative_odd(self):
        self.consumer.change_image_compress_factor(-9999999)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_positive_even_float_big(self):
        self.consumer.change_image_compress_factor(436346346.0002)
        self.assertEqual(436346346.0002, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_positive_even_float_small(self):
        self.consumer.change_image_compress_factor(0.0002)
        self.assertEqual(0.0002, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_positive_odd_float_big(self):
        self.consumer.change_image_compress_factor(17.5325)
        self.assertEqual(17.5325, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_positive_odd_float_small(self):
        self.consumer.change_image_compress_factor(0.00000000000009)
        self.assertEqual(0.00000000000009, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_0_even_float(self):
        self.consumer.change_image_compress_factor(-0.00000)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_0_odd_float(self):
        self.consumer.change_image_compress_factor(0.0000000000)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_negative_even_big(self):
        self.consumer.change_image_compress_factor(-532525234.787882)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_negative_even_small(self):
        self.consumer.change_image_compress_factor(-0.787883)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_negative_odd_big(self):
        self.consumer.change_image_compress_factor(-9999998.24)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_change_image_compress_factor_to_negative_odd_small(self):
        self.consumer.change_image_compress_factor(-0.00000000000001)
        self.assertEqual(1, self.consumer._image_compress_factor)

    def test_compress_image_init_one_channel_even(self):
        source = Source((500, 500, 1)).get_data()
        self.assertEqual((250, 250), self.consumer._compress_image(source).shape)

    def test_compress_image_init_one_channel_odd(self):
        source = Source((501, 501, 1)).get_data()
        self.assertEqual((250, 250), self.consumer._compress_image(source).shape)

    def test_compress_image_init_three_channel_even(self):
        source = Source((500, 500, 3)).get_data()
        self.assertEqual((250, 250, 3), self.consumer._compress_image(source).shape)

    def test_compress_image_init_three_channel_odd(self):
        source = Source((501, 501, 3)).get_data()
        self.assertEqual((250, 250, 3), self.consumer._compress_image(source).shape)

    def test_compress_image_positive_one_channel_big(self):
        self.consumer.change_image_compress_factor(5000000)
        source = Source((501, 501, 1)).get_data()
        self.assertEqual((1, 1), self.consumer._compress_image(source).shape)

    def test_compress_image_positive_three_channel_big(self):
        self.consumer.change_image_compress_factor(5000000)
        source = Source((500, 500, 3)).get_data()
        self.assertEqual((1, 1, 3), self.consumer._compress_image(source).shape)

    def test_compress_image_positive_one_channel_small(self):
        self.consumer.change_image_compress_factor(0.1)
        source = Source((501, 501, 1)).get_data()
        self.assertEqual((5010, 5010), self.consumer._compress_image(source).shape)

    def test_compress_image_positive_three_channel_small(self):
        self.consumer.change_image_compress_factor(0.1)
        source = Source((500, 500, 3)).get_data()
        self.assertEqual((5000, 5000, 3), self.consumer._compress_image(source).shape)

    def test_run_stop(self):
        source = Source((100, 100, 3))
        stack_a = LifoQueue(maxsize=10)
        for i in range(10):
            stack_a.put(source.get_data())

        stack_b = LifoQueue(maxsize=10)
        consumer = Consumer(stack_a, stack_b)

        while not stack_b.full():
            pass

        consumer.stop()
        time.sleep(0.5)
        self.assertEqual(False, consumer.is_alive())
