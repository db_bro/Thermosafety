from queue import LifoQueue


def queue_to_list(stack: LifoQueue) -> list:
    return list(stack.queue)
