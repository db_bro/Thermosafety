from os import path
import os.path


def check_and_make_dir(dir_name: str) -> None:
    if not path.exists(dir_name):
        os.mkdir(dir_name)
