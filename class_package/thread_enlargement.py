from threading import Thread
from queue import LifoQueue


class ThreadEnlargement(Thread):
    def __init__(self, stack_a: LifoQueue,
                 thread_name: str,
                 daemon: bool = True,
                 ):
        self._stack_a: LifoQueue = stack_a
        self._stop_flag: bool = False

        Thread.__init__(self)
        self.name = thread_name
        self.daemon: bool = daemon
        self.start()

    def stop(self) -> None:
        self._stop_flag = True
