from class_package.thread_enlargement import ThreadEnlargement
from class_package.source import Source
from time import sleep


class Producer(ThreadEnlargement):
    def __init__(self, stack_a,
                 daemon: bool = True,
                 time_send: float = 0.05,
                 source_shape: tuple = (1024, 768, 3),
                 thread_name: str = "ProducerThread",
                 ):
        self._time_send: float = time_send
        self._source: Source = Source(source_shape)

        ThreadEnlargement.__init__(self, stack_a, thread_name, daemon)

    def run(self) -> None:
        while not self._stop_flag:
            sleep(self._time_send)
            self._stack_a.put(self._source.get_data())

    def change_time_send(self, time_send) -> None:
        self._time_send = time_send if time_send > 0 else 1
