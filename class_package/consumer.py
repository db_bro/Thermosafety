from class_package.thread_enlargement import ThreadEnlargement
from queue import LifoQueue
import numpy as np
import cv2


class Consumer(ThreadEnlargement):
    def __init__(self, stack_a: LifoQueue,
                 stack_b: LifoQueue,
                 daemon: bool = True,
                 thread_name: str = "ConsumerThread",
                 ):
        self._stack_b: LifoQueue = stack_b
        self._kernel_median_size: int = 5
        self._image_compress_factor: float = 2

        ThreadEnlargement.__init__(self, stack_a, thread_name, daemon)

    def run(self) -> None:
        while not self._stop_flag:
            if not self._stack_b.full():
                compressed_img = self._compress_image(self._stack_a.get())
                median_img = self._apply_median_filter(compressed_img)
                self._stack_b.put(median_img)

    def change_median_kernel_size(self, size: int) -> None:
        if not size % 2:
            size = size + 1
        self._kernel_median_size = size if size > 0 else 1

    def change_image_compress_factor(self, factor: float) -> None:
        self._image_compress_factor = factor if factor > 0 else 1

    def _apply_median_filter(self, img: np.ndarray) -> np.ndarray:
        return cv2.medianBlur(img, self._kernel_median_size)

    def _compress_image(self, img: np.ndarray) -> np.ndarray:
        temp_width_compress = int(img.shape[0] / self._image_compress_factor)
        temp_height_compress = int(img.shape[1] / self._image_compress_factor)
        width_compress = temp_width_compress if temp_width_compress > 0 else 1
        height_compress = temp_height_compress if temp_height_compress > 0 else 1
        return cv2.resize(img, (width_compress, height_compress))
